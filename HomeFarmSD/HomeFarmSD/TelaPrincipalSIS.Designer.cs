﻿namespace HomeFarmSD
{
    partial class TelaPrincipalSIS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TelaPrincipalSIS));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnProducao = new System.Windows.Forms.Button();
            this.PainelDeslisante = new System.Windows.Forms.Panel();
            this.btnConfiguracao = new System.Windows.Forms.Button();
            this.btnRelatorios = new System.Windows.Forms.Button();
            this.btnEstoque = new System.Windows.Forms.Button();
            this.btnCadastros = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.producaoUserControl11 = new HomeFarmSD.ProducaoUserControl1();
            this.configuracoesUserControl11 = new HomeFarmSD.ConfiguracoesUserControl1();
            this.relatoriosUserControl11 = new HomeFarmSD.RelatoriosUserControl1();
            this.estoqueUserControl11 = new HomeFarmSD.EstoqueUserControl1();
            this.cadastrosUserControl11 = new HomeFarmSD.CadastrosUserControl1();
            this.animalUserControl11 = new HomeFarmSD.AnimalUserControl1();
            this.homeUserControl11 = new HomeFarmSD.HomeUserControl1();
            this.btnListagem = new System.Windows.Forms.Button();
            this.listagemUserControl11 = new HomeFarmSD.ListagemUserControl1();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.ForestGreen;
            this.panel1.Controls.Add(this.btnListagem);
            this.panel1.Controls.Add(this.btnProducao);
            this.panel1.Controls.Add(this.PainelDeslisante);
            this.panel1.Controls.Add(this.btnConfiguracao);
            this.panel1.Controls.Add(this.btnRelatorios);
            this.panel1.Controls.Add(this.btnEstoque);
            this.panel1.Controls.Add(this.btnCadastros);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(131, 450);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnProducao
            // 
            this.btnProducao.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnProducao.FlatAppearance.BorderSize = 0;
            this.btnProducao.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnProducao.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnProducao.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnProducao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProducao.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProducao.Location = new System.Drawing.Point(22, 93);
            this.btnProducao.Name = "btnProducao";
            this.btnProducao.Size = new System.Drawing.Size(103, 33);
            this.btnProducao.TabIndex = 6;
            this.btnProducao.Text = "Produção";
            this.btnProducao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProducao.UseVisualStyleBackColor = true;
            this.btnProducao.Click += new System.EventHandler(this.btnProducao_Click);
            // 
            // PainelDeslisante
            // 
            this.PainelDeslisante.BackColor = System.Drawing.Color.White;
            this.PainelDeslisante.Location = new System.Drawing.Point(3, 41);
            this.PainelDeslisante.Name = "PainelDeslisante";
            this.PainelDeslisante.Size = new System.Drawing.Size(10, 33);
            this.PainelDeslisante.TabIndex = 1;
            // 
            // btnConfiguracao
            // 
            this.btnConfiguracao.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnConfiguracao.FlatAppearance.BorderSize = 0;
            this.btnConfiguracao.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnConfiguracao.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnConfiguracao.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnConfiguracao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfiguracao.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfiguracao.Location = new System.Drawing.Point(23, 343);
            this.btnConfiguracao.Name = "btnConfiguracao";
            this.btnConfiguracao.Size = new System.Drawing.Size(103, 33);
            this.btnConfiguracao.TabIndex = 5;
            this.btnConfiguracao.Text = "Configurações";
            this.btnConfiguracao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfiguracao.UseVisualStyleBackColor = true;
            this.btnConfiguracao.Click += new System.EventHandler(this.btnConfiguracao_Click);
            // 
            // btnRelatorios
            // 
            this.btnRelatorios.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnRelatorios.FlatAppearance.BorderSize = 0;
            this.btnRelatorios.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnRelatorios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnRelatorios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnRelatorios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRelatorios.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorios.Location = new System.Drawing.Point(23, 293);
            this.btnRelatorios.Name = "btnRelatorios";
            this.btnRelatorios.Size = new System.Drawing.Size(103, 33);
            this.btnRelatorios.TabIndex = 4;
            this.btnRelatorios.Text = "Relatórios";
            this.btnRelatorios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelatorios.UseVisualStyleBackColor = true;
            this.btnRelatorios.Click += new System.EventHandler(this.btnRelatorios_Click);
            // 
            // btnEstoque
            // 
            this.btnEstoque.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnEstoque.FlatAppearance.BorderSize = 0;
            this.btnEstoque.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnEstoque.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnEstoque.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnEstoque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstoque.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstoque.Location = new System.Drawing.Point(23, 244);
            this.btnEstoque.Name = "btnEstoque";
            this.btnEstoque.Size = new System.Drawing.Size(103, 33);
            this.btnEstoque.TabIndex = 3;
            this.btnEstoque.Text = "Estoque";
            this.btnEstoque.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstoque.UseVisualStyleBackColor = true;
            this.btnEstoque.Click += new System.EventHandler(this.btnEstoque_Click);
            // 
            // btnCadastros
            // 
            this.btnCadastros.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnCadastros.FlatAppearance.BorderSize = 0;
            this.btnCadastros.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnCadastros.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnCadastros.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnCadastros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastros.Location = new System.Drawing.Point(22, 151);
            this.btnCadastros.Name = "btnCadastros";
            this.btnCadastros.Size = new System.Drawing.Size(103, 33);
            this.btnCadastros.TabIndex = 2;
            this.btnCadastros.Text = "Cadastros";
            this.btnCadastros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCadastros.UseVisualStyleBackColor = true;
            this.btnCadastros.Click += new System.EventHandler(this.btnCadastros_Click);
            // 
            // btnHome
            // 
            this.btnHome.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Location = new System.Drawing.Point(23, 41);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(103, 33);
            this.btnHome.TabIndex = 0;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(131, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(669, 37);
            this.panel2.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(631, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 30);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // producaoUserControl11
            // 
            this.producaoUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoUserControl11.Location = new System.Drawing.Point(132, 37);
            this.producaoUserControl11.Name = "producaoUserControl11";
            this.producaoUserControl11.Size = new System.Drawing.Size(667, 413);
            this.producaoUserControl11.TabIndex = 8;
            // 
            // configuracoesUserControl11
            // 
            this.configuracoesUserControl11.BackColor = System.Drawing.Color.White;
            this.configuracoesUserControl11.Location = new System.Drawing.Point(131, 37);
            this.configuracoesUserControl11.Name = "configuracoesUserControl11";
            this.configuracoesUserControl11.Size = new System.Drawing.Size(667, 413);
            this.configuracoesUserControl11.TabIndex = 7;
            // 
            // relatoriosUserControl11
            // 
            this.relatoriosUserControl11.BackColor = System.Drawing.Color.White;
            this.relatoriosUserControl11.Location = new System.Drawing.Point(131, 37);
            this.relatoriosUserControl11.Name = "relatoriosUserControl11";
            this.relatoriosUserControl11.Size = new System.Drawing.Size(667, 413);
            this.relatoriosUserControl11.TabIndex = 6;
            // 
            // estoqueUserControl11
            // 
            this.estoqueUserControl11.BackColor = System.Drawing.Color.White;
            this.estoqueUserControl11.Location = new System.Drawing.Point(132, 37);
            this.estoqueUserControl11.Name = "estoqueUserControl11";
            this.estoqueUserControl11.Size = new System.Drawing.Size(667, 413);
            this.estoqueUserControl11.TabIndex = 5;
            // 
            // cadastrosUserControl11
            // 
            this.cadastrosUserControl11.BackColor = System.Drawing.Color.White;
            this.cadastrosUserControl11.Location = new System.Drawing.Point(132, 37);
            this.cadastrosUserControl11.Name = "cadastrosUserControl11";
            this.cadastrosUserControl11.Size = new System.Drawing.Size(667, 413);
            this.cadastrosUserControl11.TabIndex = 4;
            // 
            // animalUserControl11
            // 
            this.animalUserControl11.BackColor = System.Drawing.Color.White;
            this.animalUserControl11.Location = new System.Drawing.Point(131, 37);
            this.animalUserControl11.Name = "animalUserControl11";
            this.animalUserControl11.Size = new System.Drawing.Size(667, 413);
            this.animalUserControl11.TabIndex = 3;
            // 
            // homeUserControl11
            // 
            this.homeUserControl11.BackColor = System.Drawing.Color.White;
            this.homeUserControl11.Location = new System.Drawing.Point(131, 37);
            this.homeUserControl11.Name = "homeUserControl11";
            this.homeUserControl11.Size = new System.Drawing.Size(667, 413);
            this.homeUserControl11.TabIndex = 2;
            // 
            // btnListagem
            // 
            this.btnListagem.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.btnListagem.FlatAppearance.BorderSize = 0;
            this.btnListagem.FlatAppearance.CheckedBackColor = System.Drawing.Color.LimeGreen;
            this.btnListagem.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LimeGreen;
            this.btnListagem.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen;
            this.btnListagem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnListagem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListagem.Location = new System.Drawing.Point(22, 205);
            this.btnListagem.Name = "btnListagem";
            this.btnListagem.Size = new System.Drawing.Size(103, 33);
            this.btnListagem.TabIndex = 7;
            this.btnListagem.Text = "Listagem";
            this.btnListagem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnListagem.UseVisualStyleBackColor = true;
            this.btnListagem.Click += new System.EventHandler(this.btnListagem_Click);
            // 
            // listagemUserControl11
            // 
            this.listagemUserControl11.BackColor = System.Drawing.Color.White;
            this.listagemUserControl11.Location = new System.Drawing.Point(131, 37);
            this.listagemUserControl11.Name = "listagemUserControl11";
            this.listagemUserControl11.Size = new System.Drawing.Size(667, 413);
            this.listagemUserControl11.TabIndex = 9;
            // 
            // TelaPrincipalSIS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listagemUserControl11);
            this.Controls.Add(this.producaoUserControl11);
            this.Controls.Add(this.configuracoesUserControl11);
            this.Controls.Add(this.relatoriosUserControl11);
            this.Controls.Add(this.estoqueUserControl11);
            this.Controls.Add(this.cadastrosUserControl11);
            this.Controls.Add(this.animalUserControl11);
            this.Controls.Add(this.homeUserControl11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TelaPrincipalSIS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TelaPrincipalSIS";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConfiguracao;
        private System.Windows.Forms.Button btnRelatorios;
        private System.Windows.Forms.Button btnEstoque;
        private System.Windows.Forms.Button btnCadastros;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel PainelDeslisante;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private HomeUserControl1 homeUserControl11;
        private AnimalUserControl1 animalUserControl11;
        private CadastrosUserControl1 cadastrosUserControl11;
        private EstoqueUserControl1 estoqueUserControl11;
        private RelatoriosUserControl1 relatoriosUserControl11;
        private ConfiguracoesUserControl1 configuracoesUserControl11;
        private System.Windows.Forms.Button btnProducao;
        private ProducaoUserControl1 producaoUserControl11;
        private System.Windows.Forms.Button btnListagem;
        private ListagemUserControl1 listagemUserControl11;
    }
}