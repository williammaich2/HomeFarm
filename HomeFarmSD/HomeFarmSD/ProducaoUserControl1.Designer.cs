﻿namespace HomeFarmSD
{
    partial class ProducaoUserControl1
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnProducaoCampos = new System.Windows.Forms.Button();
            this.btnProducaoBovinos = new System.Windows.Forms.Button();
            this.btnProducaoEquinos = new System.Windows.Forms.Button();
            this.btnProducaoOvinos = new System.Windows.Forms.Button();
            this.btnProducaoSuinos = new System.Windows.Forms.Button();
            this.producaoBovinosUserControl11 = new HomeFarmSD.ProducaoBovinosUserControl1();
            this.producaoCamposUserControl11 = new HomeFarmSD.ProducaoCamposUserControl1();
            this.producaoEquinosUserControl11 = new HomeFarmSD.ProducaoEquinosUserControl1();
            this.producaoOvinosUserControl11 = new HomeFarmSD.ProducaoOvinosUserControl1();
            this.producaoSuinosUserControl11 = new HomeFarmSD.ProducaoSuinosUserControl1();
            this.producaoHomeUserControl11 = new HomeFarmSD.ProducaoHomeUserControl1();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnProducaoSuinos);
            this.panel1.Controls.Add(this.btnProducaoOvinos);
            this.panel1.Controls.Add(this.btnProducaoEquinos);
            this.panel1.Controls.Add(this.btnProducaoBovinos);
            this.panel1.Controls.Add(this.btnProducaoCampos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(667, 29);
            this.panel1.TabIndex = 0;
            // 
            // btnProducaoCampos
            // 
            this.btnProducaoCampos.Location = new System.Drawing.Point(3, 3);
            this.btnProducaoCampos.Name = "btnProducaoCampos";
            this.btnProducaoCampos.Size = new System.Drawing.Size(75, 23);
            this.btnProducaoCampos.TabIndex = 0;
            this.btnProducaoCampos.Text = "Campos";
            this.btnProducaoCampos.UseVisualStyleBackColor = true;
            this.btnProducaoCampos.Click += new System.EventHandler(this.btnProducaoCampos_Click);
            // 
            // btnProducaoBovinos
            // 
            this.btnProducaoBovinos.Location = new System.Drawing.Point(109, 3);
            this.btnProducaoBovinos.Name = "btnProducaoBovinos";
            this.btnProducaoBovinos.Size = new System.Drawing.Size(75, 23);
            this.btnProducaoBovinos.TabIndex = 1;
            this.btnProducaoBovinos.Text = "Bovinos";
            this.btnProducaoBovinos.UseVisualStyleBackColor = true;
            this.btnProducaoBovinos.Click += new System.EventHandler(this.btnProducaoBovinos_Click);
            // 
            // btnProducaoEquinos
            // 
            this.btnProducaoEquinos.Location = new System.Drawing.Point(213, 3);
            this.btnProducaoEquinos.Name = "btnProducaoEquinos";
            this.btnProducaoEquinos.Size = new System.Drawing.Size(75, 23);
            this.btnProducaoEquinos.TabIndex = 2;
            this.btnProducaoEquinos.Text = "Equinos";
            this.btnProducaoEquinos.UseVisualStyleBackColor = true;
            this.btnProducaoEquinos.Click += new System.EventHandler(this.btnProducaoEquinos_Click);
            // 
            // btnProducaoOvinos
            // 
            this.btnProducaoOvinos.Location = new System.Drawing.Point(314, 3);
            this.btnProducaoOvinos.Name = "btnProducaoOvinos";
            this.btnProducaoOvinos.Size = new System.Drawing.Size(75, 23);
            this.btnProducaoOvinos.TabIndex = 3;
            this.btnProducaoOvinos.Text = "Ovinos";
            this.btnProducaoOvinos.UseVisualStyleBackColor = true;
            this.btnProducaoOvinos.Click += new System.EventHandler(this.btnProducaoOvinos_Click);
            // 
            // btnProducaoSuinos
            // 
            this.btnProducaoSuinos.Location = new System.Drawing.Point(420, 3);
            this.btnProducaoSuinos.Name = "btnProducaoSuinos";
            this.btnProducaoSuinos.Size = new System.Drawing.Size(75, 23);
            this.btnProducaoSuinos.TabIndex = 4;
            this.btnProducaoSuinos.Text = "Suinos";
            this.btnProducaoSuinos.UseVisualStyleBackColor = true;
            this.btnProducaoSuinos.Click += new System.EventHandler(this.btnProducaoSuinos_Click);
            // 
            // producaoBovinosUserControl11
            // 
            this.producaoBovinosUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoBovinosUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoBovinosUserControl11.Name = "producaoBovinosUserControl11";
            this.producaoBovinosUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoBovinosUserControl11.TabIndex = 1;
            // 
            // producaoCamposUserControl11
            // 
            this.producaoCamposUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoCamposUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoCamposUserControl11.Name = "producaoCamposUserControl11";
            this.producaoCamposUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoCamposUserControl11.TabIndex = 2;
            // 
            // producaoEquinosUserControl11
            // 
            this.producaoEquinosUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoEquinosUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoEquinosUserControl11.Name = "producaoEquinosUserControl11";
            this.producaoEquinosUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoEquinosUserControl11.TabIndex = 3;
            // 
            // producaoOvinosUserControl11
            // 
            this.producaoOvinosUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoOvinosUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoOvinosUserControl11.Name = "producaoOvinosUserControl11";
            this.producaoOvinosUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoOvinosUserControl11.TabIndex = 4;
            // 
            // producaoSuinosUserControl11
            // 
            this.producaoSuinosUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoSuinosUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoSuinosUserControl11.Name = "producaoSuinosUserControl11";
            this.producaoSuinosUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoSuinosUserControl11.TabIndex = 5;
            // 
            // producaoHomeUserControl11
            // 
            this.producaoHomeUserControl11.BackColor = System.Drawing.Color.White;
            this.producaoHomeUserControl11.Location = new System.Drawing.Point(0, 29);
            this.producaoHomeUserControl11.Name = "producaoHomeUserControl11";
            this.producaoHomeUserControl11.Size = new System.Drawing.Size(667, 384);
            this.producaoHomeUserControl11.TabIndex = 6;
            // 
            // ProducaoUserControl1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.producaoHomeUserControl11);
            this.Controls.Add(this.producaoSuinosUserControl11);
            this.Controls.Add(this.producaoOvinosUserControl11);
            this.Controls.Add(this.producaoEquinosUserControl11);
            this.Controls.Add(this.producaoCamposUserControl11);
            this.Controls.Add(this.producaoBovinosUserControl11);
            this.Controls.Add(this.panel1);
            this.Name = "ProducaoUserControl1";
            this.Size = new System.Drawing.Size(667, 413);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnProducaoSuinos;
        private System.Windows.Forms.Button btnProducaoOvinos;
        private System.Windows.Forms.Button btnProducaoEquinos;
        private System.Windows.Forms.Button btnProducaoBovinos;
        private System.Windows.Forms.Button btnProducaoCampos;
        private ProducaoBovinosUserControl1 producaoBovinosUserControl11;
        private ProducaoCamposUserControl1 producaoCamposUserControl11;
        private ProducaoEquinosUserControl1 producaoEquinosUserControl11;
        private ProducaoOvinosUserControl1 producaoOvinosUserControl11;
        private ProducaoSuinosUserControl1 producaoSuinosUserControl11;
        private ProducaoHomeUserControl1 producaoHomeUserControl11;
    }
}
